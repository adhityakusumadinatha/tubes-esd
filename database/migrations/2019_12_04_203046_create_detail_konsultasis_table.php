<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailKonsultasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_konsultasis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_konsultasi')->unsigned();
            $table->foreign('id_konsultasi')->references('id')->on('konsultasis');
            $table->string('orang_konsultasi');
            $table->text('isi_konsultasi');
            $table->string('waktu_konsultasi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_konsultasis');
    }
}
