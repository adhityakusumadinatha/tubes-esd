<?php

namespace App\Http\Controllers;

use App\detail_konsultasi;
use App\dokter;
use App\konsultasi;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DokterController extends Controller
{
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login_dokter')->with('alert', 'Kamu harus login dulu');
        } else {
            return view('home_dokter');
        }
    }

    public function login()
    {
        return view('login_dokter');
    }

    public function loginPost(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $data = dokter::where('email', $email)->first();
        if ($data) { //apakah email tersebut ada atau tidak
            if ($password == $data->password) {
                Session::put('id', $data->id);
                Session::put('name', $data->nama_dokter);
                Session::put('email', $data->email);
                Session::put('login', TRUE);
                $id = Session::get('id');
                $konsultasi = konsultasi::where('id_dokter', $id)->get();
                $match = dokter::find($request);
                return view('home_dokter',['konsultasi' => $konsultasi, 'match' => $match]);
            } else {
                return redirect('login_dokter')->with('alert', 'Password atau Email, Salah ! pengodisian 1');
            }
        } else {
            return redirect('login_dokter')->with('alert', 'Password atau Email, Salah! pengondisian 2');
        }
    }
    public function logout()
    {
        Session::flush();
        return redirect('login_dokter')->with('alert', 'Kamu sudah logout');
    }
    public function detail($id){
        $judul = konsultasi::where('id', $id)->get();
        $konsultasi = detail_konsultasi::with('konsultasi')->where('id_konsultasi',$id)->get();
        return view('detail_konsultasi_dokter',['konsultasi'=>$konsultasi, 'judul'=>$judul]);
    }

    public function newpost(Request $request){
        $nama = Session::get('name');
        $id = $request->konsulID;
        detail_konsultasi::create([
            'id_konsultasi' => $request->konsulID,
            'orang_konsultasi' => $nama,
            'isi_konsultasi' => $request->konten,
            'title'=>'Dokter'
        ]);
        return redirect('login_dokter/konsultasi/'.$id);
    }
}
