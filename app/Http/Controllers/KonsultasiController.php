<?php

namespace App\Http\Controllers;

use App\detail_konsultasi;
use App\dokter;
use App\konsultasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KonsultasiController extends Controller
{
    public function index(Request $request){
        $id = $request->id;
        $konsultasi = konsultasi::where('id_dokter', $id)->get();
        $match = dokter::find($request);
        return view('konsultasi',['konsultasi'=>$konsultasi, 'match'=>$match]);
    }

    public function detail($id){
        $judul = konsultasi::where('id', $id)->get();
        $konsultasi = detail_konsultasi::with('konsultasi')->where('id_konsultasi',$id)->get();
        return view('detail_konsultasi',['konsultasi'=>$konsultasi, 'judul'=>$judul]);
    }

    public function new($id){
        $dokter = dokter::where('id',$id)->get();
        return view('konsultasi_baru',['dokter'=>$dokter]);
    }

    public function upload(Request $request){
        $this->validate($request, [
            'nama'=> 'required',
            'topic'=> 'required',
            'desc'=> 'required'
        ]);

        konsultasi::create([
            'judul_konsultasi' => $request->topic,
            'nama_pasien' => $request->nama,
            'id_dokter' => $request->dokterID,
            'thread_starter'=> $request->desc
        ]);
        
        
        return redirect('/profil_dokter');
        
    }
}
