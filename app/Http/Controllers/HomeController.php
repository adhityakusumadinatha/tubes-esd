<?php

namespace App\Http\Controllers;

use App\artikel;
use App\tentang_kami_review;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $home = artikel::with('kategori_artikel')->get();
        $rekomended = artikel::with('kategori_artikel')->where('rekomendasi',1)->get();
        return view('home',['artikel'=>$home, 'rekomendasi'=>$rekomended]);
    }

    public function about(){
        $review = tentang_kami_review::get();
        $pertamax = tentang_kami_review::first()->get();
        return view('tentangkami', ['review'=>$review, 'pertama'=>$pertamax]);
    }
}
