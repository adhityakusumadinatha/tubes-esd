<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dokter extends Model
{
    protected $table = 'dokters';
    protected $fillable = ['nama_dokter', 'ttl_dokter', 'alamat_dokter', 'pendidikan_dokter', 'foto_dokter', 'nohp_dokter', 'email', 'password'];

    public function konsultasi(){
        return $this->hasOne('App\konsultasi', 'id_dokter', 'id');
    }
}
