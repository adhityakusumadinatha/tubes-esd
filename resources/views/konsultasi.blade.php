@extends('layouts.artikel_layout')

<script>
    function goBack() {
        window.history.back();
    }
</script>
@section('content')

<br><br><br><br><br>
<div class="container">

    <div class="text-center">
        @foreach($match as $m)
        <h1>Riwayat Konsultasi dengan Dokter {{ $m->nama_dokter}}</h1>
        <p>Jumlah Pasien (topic) : {{$konsultasi->count()}}</p>
        <a class="btn btn-success" href="/konsultasi/dokter/{{$m->id}}">Ayo Konsultasi!</a>
        @endforeach

    </div>
    <button onclick="goBack()" class="btn btn-primary">Kembali</button>
    <br>
    <br>
    @foreach($konsultasi as $k)
    <a href="/konsultasi/detail/{{ $k->id }}" name="judul" class="btn btn-primary">{{$k->judul_konsultasi}}</a><br> <br>
    </form>
    
    @endforeach
</div>



@endsection