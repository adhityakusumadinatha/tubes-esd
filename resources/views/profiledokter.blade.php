@extends('layouts.artikel_layout')
@section('content')
<br> <br>
<br>
<br>
<div class="container">
    <h1 class="text-center" style="font-family: 'Montserrat', sans-serif; font-style:normal;"> <strong> Profile Dokter</strong></h1>
    <br>
    <br>
    <div class="card-deck">
        <div class="card-deck">
            @foreach($dokter as $d)            
            <div class="row row-cols-1 row-cols-md-2">
                <div class="col mb-4">
                    
                    <div class="card">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <img src="/img/gambar_dokter/{{ $d->foto_dokter }}" class="card-img" alt="..." style="height:150px; border-radius: 50%; width: 150px; ">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $d->nama_dokter }}</h5>
                                    <p class="card-text">{{ $d->jenis_dokter }}</p>
                                    <p class="card-text">Biaya Konsultasi : <b>{{ $d->biaya }}</b></p>
                                    <p class="card-text">Dokter Tersedia saat : <b>{{ $d->waktu_tersedia }}</b></p>
                                    <a href="" class="btn btn-primary" data-toggle="modal" data-target="#modal{{ $d->id }}">Lihat Detail Profile</a>
                                    <a href="https://api.whatsapp.com/send?phone=62{{$d->nohp_dokter}}&text=Saya%20ingin%20konsultasi%20dok.%20Jadi%20saya%20mengalami%20:" class="btn btn-success">Konsultasi via WhatsApp!</a>
                                    <br><br>
                                    <form action="/konsultasi" method="post">
                                    {{ csrf_field() }}
                                    <input type="number" name="id" value="{{$d->id}}" hidden="true">
                                    <input type="submit" class="btn btn-primary" value="Lihat Konsultasi">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <br>
    <!-- Modal Profile Dokter-->
    @foreach($dokter as $d)
    <div class="modal fade" id="modal{{$d->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Profile Dokter {{ $d->nama_dokter }} </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- Detail Dokter-->

                    <div class="form-group">
                        <div class="row justify-content-md-center">
                            <div class="col-md-auto">
                                <img src="img/gambar_dokter/{{ $d->foto_dokter }}" alt="" width="250px">
                                <br>
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="row">
                                    <label class="col-md-2 col-form-label">Nama</label>
                                    <div class="col-md-10">
                                        <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$d->nama_dokter}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-2 col-form-label">TTL</label>
                                    <div class="col-md-10">
                                        <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$d->ttl_dokter}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-2 col-form-label">Alaamt</label>
                                    <div class="col-md-10">
                                        <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$d->alamat_dokter}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-2 col-form-label">Lulusan</label>
                                    <div class="col-md-10">
                                        <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$d->pendidikan_dokter}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-2 col-form-label">Narahubung</label>
                                    <div class="col-md-10">
                                        <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="0{{$d->nohp_dokter}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <p class="text-center"><b>Review</b></p>
                                <br>
                                @foreach ($review as $r)
                                <div class="container darker">
                                    <img src="/img/" alt="" class="right" style="width: auto;">
                                    <p> <strong>{{ $r->nama_pasien }}</strong> <br> {{$r->isi_review}}</p>
                                </div>
                                @endforeach
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
    @endforeach
</div>

@endsection