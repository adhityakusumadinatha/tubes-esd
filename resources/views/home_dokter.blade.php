@extends('layouts.artikel_layout')
@section('content')
<br><br><br><br>

<!-- Main Section -->
<section class="main-section">
    <!-- Add Your Content Inside -->
    <div class="content">
        <div class="container">
            <div class="text-center">
                <h1>Hallo, {{Session::get('name')}}. Apakabar?</h1>
            </div>
            <a href="{{ url('/login_dokter/logout') }}" class="btn btn-primary btn-lg">Logout</a>
            <br>
            <h2>Konsultasimu :</h2>
            @foreach($konsultasi as $k)
            <a href="/login_dokter/konsultasi/{{ $k->id }}" name="judul" class="btn btn-primary">{{$k->judul_konsultasi}}</a><br> <br>
            </form>

            @endforeach
        </div>
    </div>
    <!-- /.content -->
</section>
<!-- /.main-section -->
@endsection