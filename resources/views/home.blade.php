@extends('layouts.artikel_layout')
<style>
    body {
        background: linear-gradient(rgba(0, 0, 0, 0.6),
                rgba(0, 0, 0, 0.6)), url('img/background-landing-page.jpg');

        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        // Add smooth scrolling to all links
        $("a").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function() {

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    });
</script>

<!-- style video -->
<style>
    .wrapper {
        width: 500px;
        margin: 0 auto;
    }

    @keyframes fade-in-up {
        0% {
            opacity: 0;
        }

        100% {
            transform: translateY(0);
            opacity: 1;
        }
    }

    .stuck {
        position: fixed;
        bottom: 20px;
        right: 20px;
        transform: translateY(100%);
        width: 400px;
        height: 200px;
        animation: fade-in-up .25s ease forwards;
        z-index: 999;
    }

    /*Floating CSS End*/

    @keyframes example {
        0% {
            background-color: red;
        }

        25% {
            background-color: #ff7037;
        }

        50% {
            background-color: red;
        }

        100% {
            background-color: #ff7037;
        }
    }

    p.scrolldown {
        width: 200px;
        font-size: 20px;
        font-weight: bold;
        text-align: center;
        border: 1px solid;
        background: #ff7037;
        position: fixed;
        right: 75px;
        color: #fff;
        -webkit-animation-name: example;
        /* Safari 4.0 - 8.0 */
        -webkit-animation-duration: 4s;
        /* Safari 4.0 - 8.0 */
        animation-name: example;
        animation-duration: 2s;
    }
</style>

@section('content')
<div class="container">
    <div class="text-center" style="margin-top:35%; font-family: 'Montserrat', sans-serif; font-style:normal; color: white;text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);">
        <h1 style="font-size:35pt">Selamat Datang di Pakar<strong>Sehat</strong></h1>
        <br>
        <p style="font-size:20pt">Pakar<strong>Sehat</strong> adalah sebuah website<br>
            yang membahas kesehatan di bidang anak-anak
        </p>
        <br>
        <br>
        <br>
        <a href="#rekomendasi" style="color: white;" heigth:80px;><strong>V</strong></a>
        <!-- Artikel Trending -->
        <div id="artikel" style="margin-top: 30%;">
        
            <h1 id="rekomendasi">Rekomendasi Video Saat Ini</h1> <br>
            <p class="scrolldown">Scroll Down <span> &#x21E9;</span></p>
            <div class="wrapper"><iframe width="560" height="315" src="https://www.youtube.com/embed/edXmjcuWiQA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </iframe>
            </div>
            <br> <br> <br>
            <h2>Artikel Trending saat ini :</h2>
            <br>

            <div class="card-deck">
                @foreach($artikel as $a)
                <div class="card">
                    <img src="img/gambar_artikel/{{ $a->gambar_artikel}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title" style="color: black;">{{ $a->judul_artikel}}</h5>
                        <a class="card-text btn btn-primary" href="/artikel/detail/{{ $a->id }}">Detail Artikel</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <br>
        <!-- Artikel Terupdate -->
        <div id="artikel2">
            <h2>Artikel Terupdate saat ini :</h2>
            <br>
            <div class="card-deck">
                @foreach($artikel as $a)
                <div class="card">
                    <img src="img/gambar_artikel/{{ $a->gambar_artikel }}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title" style="color: black;">{{ $a->judul_artikel}}</h5>
                        <a class="card-text btn btn-primary" href="/artikel/detail/{{ $a->id }}">Detail Artikel</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <br>
        <!-- Artikel Terupdate -->
        <div id="artikel3">
            <h2>Artikel Rekomendasi saat ini :</h2>
            <br>
            <div class="card-deck">
                @foreach($rekomendasi as $r)
                <div class="card">
                    <img src="img/gambar_artikel/{{ $r->gambar_artikel }}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title" style="color: black;">{{ $r->judul_artikel}}</h5>
                        <a class="card-text btn btn-primary" href="/artikel/detail/{{ $r->id }}">Detail Artikel</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <br>
        <br>
        <a href="/artikel" class="btn btn-primary">Lihat Artikel lainnya</a>
    </div>


</div>
<!-- JS untuk video -->
<script>
    /*Floating Code for Iframe Start*/
    if (jQuery('iframe[src*="https://www.youtube.com/embed/"]').length > 0) {
        /*Wrap (all code inside div) all vedio code inside div*/
        jQuery('iframe[src*="https://www.youtube.com/embed/"]').wrap("<div class='iframe-parent-class'></div>");
        /*main code of each (particular) vedio*/
        jQuery('iframe[src*="https://www.youtube.com/embed/"]').each(function(index) {

            /*Floating js Start*/
            var windows = jQuery(window);
            var iframeWrap = jQuery(this).parent();
            var iframe = jQuery(this);
            var iframeHeight = iframe.outerHeight();
            var iframeElement = iframe.get(0);
            windows.on('scroll', function() {
                var windowScrollTop = windows.scrollTop();
                var iframeBottom = iframeHeight + iframeWrap.offset().top;
                //alert(iframeBottom);

                if ((windowScrollTop > iframeBottom)) {
                    iframeWrap.height(iframeHeight);
                    iframe.addClass('stuck');
                    jQuery(".scrolldown").css({
                        "display": "none"
                    });
                } else {
                    iframeWrap.height('auto');
                    iframe.removeClass('stuck');
                }
            });
            /*Floating js End*/
        });
    }

    /*Floating Code for Iframe End*/
</script>
@endsection