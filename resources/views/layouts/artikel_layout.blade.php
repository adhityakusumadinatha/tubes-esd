<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <title>PakarSehat</title>
</head>
<style>
#navbar{
  overflow: hidden;
  padding: 20px 5px; /* Large padding which will shrink on scroll (using JS) */
  transition: 0.4s; /* Adds a transition effect when the padding is decreased */
  position: fixed; /* Sticky/fixed navbar */
  top: 0; /* At the top */
  width: 100%;
  z-index: 99;
}
/* Add responsiveness - on screens less than 580px wide, display the navbar vertically instead of horizontally */
@media screen and (max-width: 580px) {
  #navbar {
    padding: 20px 10px !important; /* Use !important to make sure that JavaScript doesn't override the padding on small screens */
  }
  #navbar a {
    float: none;
    display: block;
    text-align: left;
  }
  #navbar-right {
    float: none;
  }
}
</style>
<script>
// When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("navbar").style.padding = "10px 5px";
    document.getElementById("logo").style.fontSize = "25px";
  } else {
    document.getElementById("navbar").style.padding = "20px 5px";
    document.getElementById("logo").style.fontSize = "30px";
  }
}
</script>
<body>
    <!-- Navbar -->
<nav class="navbar navbar-expand-lg" style="background-color: #F1C40F;" id="navbar">
  <a class="navbar-brand" href="/home" style="font-family: 'Montserrat', sans-serif; font-size: 30px; font-style:normal; color: white" id="logo">Pakar<strong>Sehat</strong></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{ url('home') }}" style="font-family: 'Montserrat', sans-serif; font-size: 15pt; font-style:normal; {{ Request::is('home') ? 'color: white; border-bottom: 2px solid white' : 'color: rgba(255, 255, 255, 0.5);'}}">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('profil_dokter') }}" style="font-family: 'Montserrat', sans-serif; font-size: 15pt; font-style:normal; {{ Request::is('profil_dokter') ? 'color: white; border-bottom: 2px solid white' : 'color: rgba(255, 255, 255, 0.5);'}}">Profil Dokter</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('artikel') }}" style="font-family: 'Montserrat', sans-serif; font-size: 15pt; font-style:normal; {{ Request::is('artikel') ? 'color: white; border-bottom: 2px solid white' : 'color: rgba(255, 255, 255, 0.5);'}}">Artikel</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('tentangkami') }}" style="font-family: 'Montserrat', sans-serif; font-size: 15pt; font-style:normal;{{ Request::is('tentangkami') ? 'color: white; border-bottom: 2px solid white' : 'color: rgba(255, 255, 255, 0.5);'}}">Tentang Kami</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0" action="/artikel/cari" method="POST" role="search">
      {{csrf_field()}}
      <input class="form-control mr-sm-2" type="search" placeholder="Cari Artikel.." aria-label="Search" name="search">
      <button class="btn btn-success my-2 my-sm-0" type="submit"><img src="img\svg\search.svg" alt="Logo Search" height="20px"></button>
    </form>
  </div>
</nav>
<main class="py-4">
            @yield('content')
        </main>


<script src="/js/jquery-3.4.1.min.js"></script>
<script src="/js/bootstrap.bundle.js"></script>
</body>
</html>