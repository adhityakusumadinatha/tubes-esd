@extends('layouts.artikel_layout')

<script>
    function goBack() {
        window.history.back();
    }
</script>
@section('content')
<div class="container">
<br>
<br>
<br>
<br>
    <button onclick="goBack()" class="btn btn-primary">Kembali</button>
    @foreach($judul as $j)
    <h2>{{$j->judul_konsultasi}}</h2>
    
    <div class="card" style="width: auto">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h5 class="card-title">{{$j->nama_pasien}}</h5>
                </div>
            </div>
            <p class="card-text" style="text-align: justify"><?php $isi = "{$j->thread_starter}";
                                                                echo '' . $isi; ?></p>
        </div>
    </div>
    @endforeach

    @foreach($konsultasi as $d)

    <div class="card" style="width: auto">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h5 class="card-title">{{$d->orang_konsultasi}}</h5>
                </div>
                <div class="col">
                    <p> <span class="float-right">{{$d->waktu_konsultasi}}</span></p>
                </div>
            </div>
            <h6 class="card-subtitle mb-2 text-muted">{{$d->title}}</h6>
            <p class="card-text" style="text-align: justify"><?php $isi = "{$d->isi_konsultasi}";
                                                                echo '' . $isi; ?></p>
        </div>
    </div>
    @endforeach
</div>

@endsection