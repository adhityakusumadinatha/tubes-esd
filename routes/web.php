<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index');
Route::get('/profil_dokter', 'ProfileController@index');
Route::get('/profil_dokter/detail', 'ProfileController@detail');
Route::get('/tentangkami', 'HomeController@about');
Route::get('/artikel', 'ArtikelController@index');
Route::get('/artikel/detail/{id}', 'ArtikelController@detail');
Route::post('/konsultasi', 'KonsultasiController@index');
Route::get('/konsultasi/detail/{id}', 'KonsultasiController@detail');
Route::any('/artikel/cari', 'ArtikelController@cari');
Route::get('/konsultasi/dokter/{id}', 'KonsultasiController@new');
Route::post('/konsultasi/upload', 'KonsultasiController@upload');
Route::get('/login_dokter', 'DokterController@login');
Route::post('/login_dokter/login', 'DokterController@loginPost');
Route::any('/login_dokter/logout', 'DokterController@logout');
Route::post('/login_dokter/newpost', 'DokterController@newpost');
Route::any('/login_dokter/konsultasi/{id}', 'DokterController@detail');